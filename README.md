# css-variable-in-js
Change CSS variables using Javascript.

## License
[Unlicense](https://unlicense.org/)

## Live Demo
[https://projects.gregoryhammond.ca/css-variable-in-js/](https://projects.gregoryhammond.ca/css-variable-in-js/)